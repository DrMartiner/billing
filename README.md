# Run tests
```bash
export ROLE=debug
export POSTGRES_DB=billing
export POSTGRES_USER=user
export POSTGRES_PASSWORD=password
export POSTGRES_HOST=localhost

pipenv run ./manage.py migrate
pipenv run ./manage.py loaddata project/fixtures/$ROLE/*

pipenv run ./manage.py test
```