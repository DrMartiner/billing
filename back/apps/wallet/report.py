import abc
import csv
from pathlib import Path

from django.conf import settings

from apps.wallet.models import Report, Transfer


class ITransferExport(abc.ABC):
    _report: Report
    _file = None
    _writer = None

    def __init__(self, report: Report):
        self._report = report

    def __enter__(self):
        self._open_file()
        return self

    def _open_file(self):
        folder_path = Path(settings.MEDIA_ROOT) / 'reports'
        folder_path.mkdir(parents=True, exist_ok=True)

        self._file = open(str(folder_path / self.filename), mode='w')

    def __exit__(self, type, value, traceback):
        self._file.close()

    @abc.abstractmethod
    def write_row(self, transfer: Transfer):
        pass

    @property
    def filename(self) -> str:
        return f'{self._report.uuid}.{self._report.type}'


class CsvTransferExport(ITransferExport):
    def __enter__(self):
        self._open_file()

        fieldnames = ['id', 'user_to', 'currency_to', 'amount', 'created']
        self._writer = csv.DictWriter(self._file, fieldnames=fieldnames)
        self._writer.writeheader()

        return self

    def write_row(self, transfer: Transfer):
        self._writer.writerow({
            'id': transfer.id,
            'user_to': transfer.user_to.username,
            'currency_to': transfer.user_to.currency,
            'amount': transfer.amount,
            'created': transfer.created.isoformat(),
        })
