from uuid import uuid4

from django.db import models


class Deposit(models.Model):
    user = models.ForeignKey('users.User', on_delete=models.CASCADE)
    amount = models.DecimalField(max_digits=10, decimal_places=2)

    created = models.DateTimeField(auto_now_add=True, db_index=True)

    class Meta:
        app_label = 'wallet'
        ordering = ['-created']

    def __str__(self):
        return f'To={self.user.pk} Amount={self.amount}'


class Transfer(models.Model):
    user_from = models.ForeignKey('users.User', on_delete=models.CASCADE, related_name='user_from')
    user_to = models.ForeignKey('users.User', on_delete=models.CASCADE, related_name='user_to')
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    course = models.ForeignKey('Course', on_delete=models.CASCADE)

    created = models.DateTimeField(auto_now_add=True, db_index=True)

    class Meta:
        app_label = 'wallet'
        ordering = ['-created']

    def __str__(self):
        return f'From={self.user_from.pk} To={self.user_to.pk} Amount={self.amount}'


class Course(models.Model):
    usd = 1
    eur = models.DecimalField(max_digits=8, decimal_places=2)
    cad = models.DecimalField(max_digits=8, decimal_places=2)
    cny = models.DecimalField(max_digits=8, decimal_places=2)

    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        app_label = 'wallet'
        ordering = ['-created']


class Report(models.Model):
    class TYPE:
        CSV = 'csv'
        XML = 'xml'

        CHOICES = (
            (CSV, 'CSV'),
            (XML, 'XML'),
        )

    uuid = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    owner = models.ForeignKey('users.User', on_delete=models.CASCADE, related_name='owner')
    type = models.CharField(max_length=3, choices=TYPE.CHOICES)
    user_for = models.ForeignKey('users.User', on_delete=models.CASCADE, related_name='user_for')
    date_from = models.DateTimeField(blank=True, null=True)
    date_to = models.DateTimeField(blank=True, null=True)
    file = models.FileField(blank=True, null=True, upload_to='reports')

    is_done = models.BooleanField(default=False)

    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        app_label = 'wallet'
        ordering = ['-created']
