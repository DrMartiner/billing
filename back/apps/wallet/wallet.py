import logging
from _decimal import Decimal

from django.db import transaction, connection
from django.db.models import F

from apps.common.fields import CurrencyField
from .exceptions import InsufficientBalance, CourseDoesNotExists
from .models import Deposit, Transfer, Course
from apps.users.models import User

__all__ = ['Wallet']

logger = logging.getLogger('django.request')


class Wallet:
    @staticmethod
    @transaction.atomic
    def deposit(user_id: int, amount: Decimal) -> Deposit:
        user_queryset = User.objects.filter(id=user_id).select_for_update()
        user_queryset.update(balance=F('balance') - amount)

        deposit = Deposit.objects.create(user_id=user_id, amount=amount)
        return deposit

    @classmethod
    @transaction.atomic
    def transfer(cls, user_from_id: int, user_to_id: int, amount: Decimal) -> Transfer:
        # Decrease user_from balance
        user_from_queryset = User.objects.filter(id=user_from_id).select_for_update()
        if user_from_queryset.first().balance < amount:
            raise InsufficientBalance(f'UserID={user_from_queryset.first().id} has insufficient balance')
        user_from_queryset.update(balance=F('balance') - amount)

        # Increase user_to balance
        user_to_queryset = User.objects.filter(id=user_to_id).select_for_update()
        cross_course, course = cls.get_cross_course(
            user_from_queryset.first().currency,
            user_to_queryset.first().currency
        )
        user_to_queryset.update(balance=F('balance') + amount * cross_course)

        # Create Transfer
        return Transfer.objects.create(
            user_from_id=user_from_id,
            user_to_id=user_to_id,
            amount=amount,
            course=course
        )

    @staticmethod
    @transaction.atomic
    def get_cross_course(symbol_from: str, symbol_to: str) -> tuple:
        if CurrencyField.check_symbol_allowed(symbol_from):
            raise ValueError(f'currency_from "{symbol_from}" is not allowed')

        if CurrencyField.check_symbol_allowed(symbol_to):
            raise ValueError(f'currency_to "{symbol_to}" is not allowed')

        course = Course.objects.last()
        if not course:
            raise CourseDoesNotExists()

        if symbol_from == symbol_to:
            return 1., course

        cross_course = Decimal(getattr(course, symbol_from) / getattr(course, symbol_to))
        return round(cross_course, 2), course
