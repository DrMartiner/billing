class InsufficientBalance(Exception):
    pass


class CourseDoesNotExists(Exception):
    pass
