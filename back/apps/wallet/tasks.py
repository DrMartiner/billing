import requests
from celery import shared_task
from celery.schedules import crontab
from celery.task import periodic_task
from chunkator import chunkator
from django.db.models import Q

from .models import Report, Transfer, Course
from .report import CsvTransferExport


@shared_task(queue='report')
def make_report(report_id: int):
    report = Report.objects.get(pk=report_id)

    filters = Q()
    filters.add(Q(user_from__id=report.user_for.id), Q.AND)
    if report.date_from:
        filters.add(Q(created__lte=report.date_from), Q.AND)
    if report.date_to:
        filters.add(Q(created__gte=report.date_to), Q.AND)

    if report.type == Report.TYPE.CSV:
        report_class = CsvTransferExport
    else:
        raise TypeError(f'Unknown report type: "{report.type}"')

    with report_class(report) as reporter:
        queryset = Transfer.objects.filter(filters)
        for transfer in chunkator(queryset, 2):
            reporter.write_row(transfer)


@periodic_task(queue='course', run_every=(crontab(minute='*/1')))
def load_course():
    url = 'https://api.exchangeratesapi.io/latest?base=USD&symbols=EUR,CAD,CNY'
    response = requests.get(url)

    data = response.json()
    Course.objects.create(
        eur=data['rates']['EUR'],
        cad=data['rates']['CAD'],
        cny=data['rates']['CNY'],
    )
