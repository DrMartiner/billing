import logging
from multiprocessing import Process
from time import sleep

from django_dynamic_fixture import G
from django.test.testcases import TestCase

from apps.common.utils import get_object_or_none
from apps.users.models import User

logger = logging.getLogger('concurrency')


class BaseTest(TestCase):
    maxDiff = 5000

    email = 'user@test.com'
    password = '123-123-123'
    username = 'username'

    _CONCURRENCY_DELAY = 0.5

    def setUp(self):
        super(BaseTest, self).setUp()

        self.user = self.create_user(username=self.username, email=self.email, password=self.password, balance=100)

    def create_user(self, password: str = None, **kwargs) -> User:
        user: User = G(User, **kwargs)
        if password:
            user.set_password(password)
        user.save()

        return user

    def reload_user_instance(self) -> None:
        self.user: User = get_object_or_none(User, pk=self.user.pk)

    def update_user_instance(self, **kwargs) -> None:
        self.user.__dict__.update(kwargs)
        self.user.save()

    def run_concurrency(self, func, count: int, workers_kwargs):
        def worker(number: int, **kwargs):
            try:
                result = func(**kwargs)
                logger.debug(f'Thread {number}: result:\t{result}')
            except Exception as e:
                logger.debug(f'Thread {number}: error:\t{e}')

        processes = [Process(target=worker, args=[i], kwargs=workers_kwargs[i]) for i in range(count)]

        for p in processes:
            sleep(self._CONCURRENCY_DELAY)
            p.start()

        [p.join() for p in processes]
