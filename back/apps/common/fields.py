from django.db import models

__all__ = ['CurrencyField']


class CurrencyField(models.CharField):
    USD = 'usd'
    EUR = 'eur'
    CAD = 'cad'
    CNY = 'cny'

    CHOICES = (
        (USD, 'USD'),
        (EUR, 'EUR'),
        (CAD, 'CAD'),
        (CNY, 'CNY'),
    )

    def __init__(self, *args, **kwargs):
        kwargs.update({
            'choices': self.CHOICES,
            'max_length': 3,
            'default': self.USD,
        })
        super(CurrencyField, self).__init__(*args, **kwargs)

    @classmethod
    def check_symbol_allowed(cls, symbol: str) -> None:
        if symbol not in [c[0] for c in cls.CHOICES]:
            raise ValueError(f'Currency "{symbol}" is not allowed')
