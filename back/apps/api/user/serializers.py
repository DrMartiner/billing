from rest_framework import serializers

from apps.users.models import User


class SelfProfileRetrieveSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ['id', 'username', 'currency', 'balance', 'first_name', 'last_name', 'country', 'city']


class SelfProfileUpdateSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'country', 'city']
