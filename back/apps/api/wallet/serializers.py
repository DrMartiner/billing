from rest_framework import serializers

from apps.users.models import User
from apps.wallet.models import Deposit, Transfer, Course, Report
from .fields import UserField


class CourseSerializer(serializers.ModelSerializer):

    class Meta:
        model = Course
        exclude = ['id']


class BalanceSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ['balance']


class CreateDepositSerializer(serializers.Serializer):
    amount = serializers.DecimalField(max_digits=10, decimal_places=2, required=True)


class DetailDepositSerializer(serializers.ModelSerializer):

    class Meta:
        model = Deposit
        fields = ['id', 'amount', 'created']


class CreateTransferSerializer(serializers.Serializer):
    user_to = UserField(required=True)
    amount = serializers.DecimalField(max_digits=10, decimal_places=2, required=True)


class DetailTransferSerializer(serializers.ModelSerializer):

    class Meta:
        model = Transfer
        fields = '__all__'


class ReportSerializer(serializers.ModelSerializer):

    class Meta:
        model = Report
        exclude = ['owner']
        read_only_fields = ['id', 'file', 'created', 'is_done']
