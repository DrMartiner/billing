from datetime import timedelta

from django.urls import reverse
from django.utils import timezone
from django_dynamic_fixture import G

from apps.api.base_test import BaseApiTest
from apps.users.models import User
from apps.wallet.models import Report, Transfer


class ReadReportTest(BaseApiTest):
    def test_succeed(self):
        user_for = G(User)

        for i in range(3):
            G(Transfer, user_from=user_for, created=timezone.now() - timedelta(days=2))

        report: Report = G(
            Report,
            is_done=True,
            owner=self.user,
            type=Report.TYPE.CSV,
            date_from=timezone.now() - timedelta(days=1),
            date_to = timezone.now() - timedelta(days=3)
        )

        self.client.force_login(self.user)

        url = reverse('api_v1:wallet:report-detail', args=[report.pk])
        response = self.client.get(url)
