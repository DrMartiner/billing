from django.urls import reverse

from apps.api.base_test import BaseApiTest
from apps.api.wallet import serializers


class GetBalanceTest(BaseApiTest):
    url = reverse('api_v1:wallet:balance')

    def test_read(self):
        self.update_user_instance(balance=100500.9999)
        self.client.force_login(self.user)

        response = self.client.get(self.url)
        self.assertResponseStatus200(response)

        self.assertRetrieveInResponse(serializers.BalanceSerializer, self.user, response, self.user)

    def test_un_auth(self):
        response = self.client.get(self.url)
        self.assertResponseStatus403(response)
