from django.urls import reverse
from django_dynamic_fixture import G

from apps.api.base_test import BaseApiTest
from apps.api.wallet import serializers
from apps.wallet.models import Report


class ReadReportListTest(BaseApiTest):
    url = reverse('api_v1:wallet:report')

    def test_succeed(self):
        for i in range(3):
            G(Report, owner=self.user)
        G(Report)

        self.client.force_login(self.user)

        response = self.client.get(self.url)
        self.assertResponseStatus200(response)

        queryset = Report.objects.filter(owner=self.user)
        self.assertListInResponse(serializers.ReportSerializer, queryset, response, self.user)

    def test_un_auth(self):
        response = self.client.get(self.url)
        self.assertResponseStatus403(response)
