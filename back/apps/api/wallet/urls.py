from django.urls import path

from . import views

urlpatterns = [
    path('course/', views.CourseView.as_view(), name='course'),
    path('balance/', views.BalanceView.as_view(), name='balance'),
    path('deposit/', views.CreateDepositView.as_view(), name='deposit-create'),
    path('transfer/', views.CreateTransferView.as_view(), name='transfer-create'),
    path('report/', views.CreateReportView.as_view(), name='report'),
    path('report/<slug:pk>/', views.DetailReportView.as_view(), name='report-detail'),
]
