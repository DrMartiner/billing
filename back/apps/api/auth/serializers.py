from rest_framework import serializers

from apps.users.models import User


class SignUpSerializer(serializers.ModelSerializer):
    password = serializers.CharField(required=True)

    class Meta:
        model = User
        fields = ['username', 'password', 'currency', 'first_name', 'last_name', 'country', 'city']


class SignInSerializer(serializers.Serializer):
    username = serializers.SlugField(max_length=150, required=True)
    password = serializers.CharField(required=True)
