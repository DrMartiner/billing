import os
from logging import config

from django.utils.translation import gettext_lazy as _

from .env import env, root

os.sys.path.insert(0, root())
os.sys.path.insert(0, os.path.join(root(), 'apps'))

ROLE = env('ROLE')

DEBUG = env('DEBUG')

SECRET_KEY = env('SECRET_KEY')

ALLOWED_HOSTS = env('ALLOWED_HOSTS')

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': os.environ.get('POSTGRES_DB'),
        'USER': os.environ.get('POSTGRES_USER'),
        'PASSWORD': os.environ.get('POSTGRES_PASSWORD'),
        'HOST': os.environ.get('POSTGRES_HOST'),

        'ATOMIC_REQUESTS': True,
        'AUTOCOMMIT': True,
    }
}

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.sites',
    'django.contrib.staticfiles',

    'django_filters',
    'drf_yasg',
    'flex',
    'rest_framework',

    'apps.common.apps.CommonConfig',
    'apps.users.apps.UsersConfig',
    'apps.wallet.apps.WalletConfig',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.static',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

ROOT_URLCONF = 'project.urls'

WSGI_APPLICATION = 'project.wsgi.application'

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

USE_I18N = True
USE_L10N = True

SITE_ID = 1

LANGUAGE_CODE = 'en'
LANGUAGES = [
    ('en', _('English')),
]

USE_TZ = True
TIME_ZONE = 'Europe/Berlin'

MEDIA_URL = '/media/'
MEDIA_ROOT = root('media')

STATIC_URL = '/static/'
STATIC_ROOT = root('static')

STATICFILES_DIRS = [
    root('project/static')
]

AUTH_USER_MODEL = 'users.User'

# Load loggers
logger_config_path = root('.loggers.ini')
if not os.path.exists(logger_config_path):
    logger_config_path = root(f'project/loggers/{ROLE}.ini')

config.fileConfig(logger_config_path)
