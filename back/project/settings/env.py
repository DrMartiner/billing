import os

import environ

root = environ.Path(__file__, '../../..')
env = environ.Env(
    DEBUG=(bool, False),
    ALLOWED_HOSTS=(list, ['*']),
    SECRET_KEY=(str, 'u=aygk6e^3z!--_49&riy6)@a#*kl$ji4_#d4gq*bu+@w7'),
    BROKER_URL=(str, 'redis://redis:6379'),
    CELERY_BROKER_URL=(str, 'redis://redis:6379'),
    CELERY_RESULT_BACKEND_URL=(str, 'redis://redis:6379'),
)

# Load env.ini
env_file_path = root('.env.ini')
if not os.path.exists(env_file_path):
    env_file_path = root('env.ini')
env.read_env(env_file_path)
